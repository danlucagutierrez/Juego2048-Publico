package codigoNegocioJuego;

import java.util.Random;

public class JuegoLogica {

	private Integer[][] tablero;
	private int tamanio;
	private Random r;

	// Constructor encargado de inicializar a "r" como un Random para ser utilizado,
	// setea "tamano" que representa las filas y columnas de la matriz,
	// el "tablero" es representado por una matriz de Integer que se inicializa en
	// 4x4.
	public JuegoLogica() {

		this.r = new Random();
		this.tamanio = 4;
		this.tablero = new Integer[this.tamanio][this.tamanio];

		for (int i = 0; i < this.tablero.length; i++) {

			for (int j = 0; j < this.tablero[0].length; j++) {

				this.tablero[i][j] = null;
			}
		}
	}

	// Constructor que se utiliza para poder hacer pruebas en la clase
	// JuegoTest,
	// este genera un tablero completo con numeros totalmente distintos para que no
	// se pueda realizar
	// sumas entre adyacentes.
	JuegoLogica(Integer x) {

		Integer contador = x;

		this.tamanio = 4;
		this.tablero = new Integer[this.tamanio][this.tamanio];

		for (int i = 0; i < this.tablero.length; i++) {

			for (int j = 0; j < this.tablero[0].length; j++) {

				this.tablero[i][j] = contador;
				contador = contador + 1;
			}
		}
	}

	// Construye el tablero de juego con 1 o 2 posiciones random con los
	// numeros 2 o 4, el resto son posiciones null.
	public void iniciarTablero() {

		int contadorRandom = this.r.nextInt(2);
		int contador = contadorRandom;

		for (int i = 0; i < this.tablero.length; i++) {

			for (int j = 0; j < this.tablero[0].length; j++) {

				if (this.tablero[i][j] == null && contador < 2) {
					this.tablero[posicionAleatoria()][posicionAleatoria()] = numeroAleatorio();
					contador++;
				}
			}
		}
	}

	// Retorna un numero random entre 0 y 3.
	private int posicionAleatoria() {

		return this.r.nextInt(4);
	}

	// Retorna un numero aleatorio 2 o 4
	private int numeroAleatorio() {

		Integer aleatorio = this.r.nextInt(5);

		if (aleatorio == 4) {
			return aleatorio;
		}
		return 2;
	}

	// Realiza las sumas cuando el usuario hace un movimiento hacia la arriba
	public void sumarHaciaArriba() {

		sacarVaciosHaciaArriba();

		Integer guardarNumero = null;
		Integer guardarPosicion = null;

		for (int i = 0; i < this.tablero.length; i++) {

			for (int j = 0; j < this.tablero[i].length; j++) {

				if (this.tablero[j][i] == null) {
					break;
				}
				if (this.tablero[j][i] != null && guardarNumero == null) {
					guardarNumero = this.tablero[j][i];
					guardarPosicion = j;
				} else if (this.tablero[j][i] != null && guardarNumero != null) {
					if (this.tablero[j][i].equals(guardarNumero)) {
						guardarNumero += this.tablero[j][i];
						this.tablero[j][i] = null;
						this.tablero[guardarPosicion][i] = guardarNumero;
						guardarNumero = null;
					} else {
						guardarNumero = this.tablero[j][i];
						guardarPosicion = j;
					}
				}
			}
			guardarNumero = null;
		}
		sacarVaciosHaciaArriba();
	}

	// Saca las posiciones vacias (null) del tablero y posiciona los numeros lo m�s
	// a la arriba posible
	private void sacarVaciosHaciaArriba() {

		Integer[][] tableroSinVacios = new Integer[this.tamanio][this.tamanio];
		int k = 0;

		for (int i = 0; i < this.tablero.length; i++) {

			for (int j = 0; j < this.tablero[i].length; j++) {

				if (this.tablero[j][i] != null) {
					tableroSinVacios[k][i] = this.tablero[j][i];
					k++;
				}
			}
			k = 0;
		}
		this.tablero = tableroSinVacios;
	}

	// Realiza las sumas cuando el usuario hace un movimiento hacia la abajo
	public void sumarHaciaAbajo() {

		sacarVaciosHaciaAbajo();

		Integer guardarNumero = null;
		Integer guardarPosicion = null;

		for (int i = this.tablero.length - 1; i >= 0; i--) {

			for (int j = this.tablero[i].length - 1; j >= 0; j--) {

				if (this.tablero[j][i] == null) {
					break;
				}
				if (this.tablero[j][i] != null && guardarNumero == null) {
					guardarNumero = this.tablero[j][i];
					guardarPosicion = j;
				} else if (this.tablero[j][i] != null && guardarNumero != null) {
					if (this.tablero[j][i].equals(guardarNumero)) {
						guardarNumero += this.tablero[j][i];
						this.tablero[j][i] = null;
						this.tablero[guardarPosicion][i] = guardarNumero;
						guardarNumero = null;
					} else {
						guardarNumero = this.tablero[j][i];
						guardarPosicion = j;
					}
				}
			}
			guardarNumero = null;
		}
		sacarVaciosHaciaAbajo();
	}

	// Saca las posiciones vacias (null) del tablero y posiciona los numeros lo m�s
	// a la abajo posible
	private void sacarVaciosHaciaAbajo() {

		Integer[][] tableroSinVacios = new Integer[this.tamanio][this.tamanio];
		int k = 3;

		for (int i = this.tablero.length - 1; i >= 0; i--) {

			for (int j = this.tablero[i].length - 1; j >= 0; j--) {

				if (this.tablero[j][i] != null) {
					tableroSinVacios[k][i] = this.tablero[j][i];
					k--;
				}
			}
			k = 3;
		}
		this.tablero = tableroSinVacios;
	}

	// Realiza las sumas cuando el usuario hace un movimiento hacia la derecha
	public void sumarHaciaLaDerecha() {

		sacarVaciosHaciaLaDerecha();

		Integer guardarNumero = null;
		Integer guardarPosicion = null;

		for (int i = 0; i < this.tablero.length; i++) {

			for (int j = this.tablero.length - 1; j >= 0; j--) {

				if (this.tablero[i][j] == null) {
					break;
				}
				if (this.tablero[i][j] != null && guardarNumero == null) {
					guardarNumero = this.tablero[i][j];
					guardarPosicion = j;
				} else if (this.tablero[i][j] != null && guardarNumero != null) {
					if (this.tablero[i][j].equals(guardarNumero)) {
						guardarNumero += this.tablero[i][j];
						this.tablero[i][j] = null;
						this.tablero[i][guardarPosicion] = guardarNumero;
						guardarNumero = null;
					} else {
						guardarNumero = this.tablero[i][j];
						guardarPosicion = j;
					}
				}
			}
			guardarNumero = null;
		}
		sacarVaciosHaciaLaDerecha();
	}

	// Saca las posiciones vacias (null) del tablero y posiciona los numeros lo m�s
	// a la derecha posible
	private void sacarVaciosHaciaLaDerecha() {

		Integer[][] tableroSinVacios = new Integer[this.tamanio][this.tamanio];
		int k = 3;

		for (int i = 0; i < this.tablero.length; i++) {

			for (int j = this.tablero.length - 1; j >= 0; j--) {

				if (this.tablero[i][j] != null) {
					tableroSinVacios[i][k] = this.tablero[i][j];
					k--;
				}
			}
			k = 3;
		}
		this.tablero = tableroSinVacios;
	}

	// Realiza las sumas cuando el usuario hace un movimiento hacia la izquierda
	public void sumarHaciaLaIzquierda() {

		sacarVaciosHaciaLaIzquierda();

		Integer guardarNumero = null;
		Integer guardarPosicion = null;

		for (int i = 0; i < this.tablero.length; i++) {

			for (int j = 0; j < this.tablero.length; j++) {

				if (this.tablero[i][j] == null) {
					break;
				}
				if (this.tablero[i][j] != null && guardarNumero == null) {
					guardarNumero = this.tablero[i][j];
					guardarPosicion = j;
				} else if (this.tablero[i][j] != null && guardarNumero != null) {
					if (this.tablero[i][j].equals(guardarNumero)) {
						guardarNumero += this.tablero[i][j];
						this.tablero[i][j] = null;
						this.tablero[i][guardarPosicion] = guardarNumero;
						guardarNumero = null;
					} else {
						guardarNumero = this.tablero[i][j];
						guardarPosicion = j;
					}
				}
			}
			guardarNumero = null;
		}
		sacarVaciosHaciaLaIzquierda();
	}

	// Saca las posiciones vacias (null) del tablero y posiciona los numeros lo m�s
	// a la izquierda posible
	private void sacarVaciosHaciaLaIzquierda() {

		Integer[][] tableroSinVacios = new Integer[this.tamanio][this.tamanio];
		int k = 0;

		for (int i = 0; i < this.tablero.length; i++) {

			for (int j = 0; j < this.tablero.length; j++) {

				if (this.tablero[i][j] != null) {
					tableroSinVacios[i][k] = this.tablero[i][j];
					k++;
				}
			}
			k = 0;
		}
		this.tablero = tableroSinVacios;
	}

	// Inserta un numero aleatorio 2 o 4 dentro del tablero
	public void agregarAleatorio() {

		int primerAleatorio = posicionAleatoria();
		int segundoAleatorio = posicionAleatoria();

		while (this.tablero[primerAleatorio][segundoAleatorio] != null) {
			primerAleatorio = posicionAleatoria();
			segundoAleatorio = posicionAleatoria();
		}
		this.tablero[primerAleatorio][segundoAleatorio] = numeroAleatorio();
	}

	// Retorna true si no encuentra una posible suma entre n�meros adyacentes
	public boolean juegoTermiado() {

		if (existenSumasHorizontales() || existenSumasVerticales()) {
			return false;
		}
		return true;
	}

	// Retorna true si existe una posible suma vertical
	public boolean existenSumasVerticales() {

		for (int i = 0; i < this.tablero.length; i++) {

			for (int j = 0; j < this.tablero.length - 1; j++) {

				if (existenEspaciosVacios()) {
					return true;
				}
				if (this.tablero[j][i] == this.tablero[j + 1][i]) {
					return true;
				}
			}
		}
		return false;
	}

	// Retorna true si existe una posible suma horizontal
	public boolean existenSumasHorizontales() {

		for (int i = 0; i < this.tablero.length; i++) {

			for (int j = 0; j < this.tablero.length - 1; j++) {

				if (existenEspaciosVacios()) {
					return true;
				}
				if (this.tablero[i][j] == this.tablero[i][j + 1]) {
					return true;
				}
			}
		}
		return false;
	}

	// Retorna true si existe espacio vacio en el tablero
	private boolean existenEspaciosVacios() {

		for (int i = 0; i < this.tablero.length; i++) {

			for (int j = 0; j < this.tablero.length; j++) {

				if (this.tablero[i][j] == null) {
					return true;
				}
			}
		}
		return false;
	}

	// Retorna el tama�o del tablero
	public int getTamanio() {

		return this.tamanio;
	}

	// Retorna el numero que contiene la posicion del tablero "i"
	public Integer posicionEnTablero(int i) {

		return this.tablero[i].length;
	}

	// Retorna el numero que contiene la posicion del tablero "i" y "j"
	public Integer posicionesEnTablero(int i, int j) {

		return this.tablero[i][j];
	}
}