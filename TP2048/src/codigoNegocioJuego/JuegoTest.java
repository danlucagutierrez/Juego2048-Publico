package codigoNegocioJuego;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

public class JuegoTest {

	// Verifica tablero sin movimientos
	@Test
	public void tableroSinMovimientos() {

		JuegoLogica tablero = new JuegoLogica(1);

		assertTrue(tablero.juegoTermiado());
	}

	// Verifica tablero con algun movimiento
	@Test
	public void tableroConMovimientos() {

		JuegoLogica tablero = new JuegoLogica();
		tablero.iniciarTablero();

		assertFalse(tablero.juegoTermiado());
	}
}