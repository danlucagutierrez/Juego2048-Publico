package interfazVisualJuego;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import java.awt.SystemColor;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;
import codigoNegocioJuego.JuegoLogica;
import java.awt.event.KeyEvent;

public class JuegoVisual {

	private JFrame juego;
	private JuegoLogica tableroNuevo;
	private JLabel casilleroUno;
	private JLabel casilleroDos;
	private JLabel casilleroTres;
	private JLabel casilleroCuatro;
	private JLabel casilleroCinco;
	private JLabel casilleroSeis;
	private JLabel casilleroSiete;
	private JLabel casilleroOcho;
	private JLabel casilleroNueve;
	private JLabel casilleroDiez;
	private JLabel casilleroOnce;
	private JLabel casilleroDoce;
	private JLabel casilleroTrece;
	private JLabel casilleroQuince;
	private JLabel casilleroCatorce;
	private JLabel casilleroDieciseis;
	private ArrayList<JLabel> casilleros;
	private JLabel mensajeJuegoTerminado;
	private boolean juegoIniciado;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JuegoVisual window = new JuegoVisual();
					window.juego.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public JuegoVisual() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		// Frame que contiene el juego
		juego = new JFrame();
		juego.setTitle("Game 2048");
		juego.getContentPane().setBackground(SystemColor.text);
		juego.setBounds(440, 50, 530, 660);
		juego.getContentPane().setLayout(null);
		juego.setResizable(false); // Tama�o fijo de vetana de juego

		// Titulo para el nombre del juego
		JLabel nombreJuego = new JLabel("2048");
		nombreJuego.setForeground(SystemColor.windowBorder);
		nombreJuego.setFont(new Font("Arial", Font.BOLD, 45));
		nombreJuego.setBounds(10, 7, 100, 58);
		juego.getContentPane().add(nombreJuego);

		// Titulo por debajo del nombre del juego
		JLabel tituloJuego = new JLabel("Play 2048 Desktop Game");
		tituloJuego.setForeground(SystemColor.windowBorder);
		tituloJuego.setFont(new Font("Arial", Font.BOLD, 15));
		tituloJuego.setBounds(10, 76, 173, 16);
		juego.getContentPane().add(tituloJuego);

		// Subtitulo con mensaje de informaci�n sobre el juego
		JLabel subtituloJuego = new JLabel("Join the numbers and get to the");
		subtituloJuego.setForeground(SystemColor.windowBorder);
		subtituloJuego.setFont(new Font("Arial", Font.PLAIN, 15));
		subtituloJuego.setBounds(10, 92, 267, 17);
		juego.getContentPane().add(subtituloJuego);

		// Subtitulo que conecta a "subtituloJuego"
		JLabel subtituloJuegoDos = new JLabel("2048 tile!");
		subtituloJuegoDos.setForeground(SystemColor.windowBorder);
		subtituloJuegoDos.setFont(new Font("Arial", Font.BOLD, 15));
		subtituloJuegoDos.setBounds(216, 93, 62, 14);
		juego.getContentPane().add(subtituloJuegoDos);

		// Se utiliza como bandera
		juegoIniciado = false;

		// Bot�n para iniciar el juego
		JButton botonInicioJuego = new JButton("New Game");
		botonInicioJuego.setBackground(new Color(153, 153, 102));
		botonInicioJuego.setForeground(SystemColor.window);
		botonInicioJuego.setFocusable(false);

		// Al accionar el boton se inicia el tablero del juego
		botonInicioJuego.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				tableroNuevo = new JuegoLogica();
				tableroNuevo.iniciarTablero();

				casilleros = new ArrayList<>();
				casilleros.add(casilleroUno);
				casilleros.add(casilleroDos);
				casilleros.add(casilleroTres);
				casilleros.add(casilleroCuatro);
				casilleros.add(casilleroCinco);
				casilleros.add(casilleroSeis);
				casilleros.add(casilleroSiete);
				casilleros.add(casilleroOcho);
				casilleros.add(casilleroNueve);
				casilleros.add(casilleroDiez);
				casilleros.add(casilleroOnce);
				casilleros.add(casilleroDoce);
				casilleros.add(casilleroTrece);
				casilleros.add(casilleroCatorce);
				casilleros.add(casilleroQuince);
				casilleros.add(casilleroDieciseis);

				int contadorCasilleros = 0;

				mensajeJuegoTerminado.setVisible(false);

				for (int i = 0; i < tableroNuevo.getTamanio(); i++) {

					for (int j = 0; j < tableroNuevo.posicionEnTablero(0); j++) {

						if (tableroNuevo.posicionesEnTablero(i, j) == null) {
							casilleros.get(contadorCasilleros).setText("");
							contadorCasilleros++;
						} else {
							casilleros.get(contadorCasilleros)
									.setText(String.valueOf(tableroNuevo.posicionesEnTablero(i, j)));
							contadorCasilleros++;
						}
					}
				}
				juegoIniciado = true;
			}
		});

		botonInicioJuego.setFont(new Font("Arial", Font.BOLD, 15));
		botonInicioJuego.setToolTipText("");
		botonInicioJuego.setBounds(383, 65, 122, 39);
		juego.getContentPane().add(botonInicioJuego);

		juego.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyReleased(java.awt.event.KeyEvent evt) {
				if (juegoIniciado == true && evt.getKeyCode() == KeyEvent.VK_UP) {

					if (tableroNuevo.juegoTermiado() == true) {
						mensajeJuegoTerminado.setVisible(true);
					} else {
						if (tableroNuevo.existenSumasVerticales() == true) {
							tableroNuevo.sumarHaciaArriba();
							tableroNuevo.agregarAleatorio();

							int contadorCasilleros = 0;

							for (int i = 0; i < tableroNuevo.getTamanio(); i++) {

								for (int j = 0; j < tableroNuevo.posicionEnTablero(0); j++) {

									if (tableroNuevo.posicionesEnTablero(i, j) == null) {
										casilleros.get(contadorCasilleros).setText("");
										contadorCasilleros++;
									} else {
										casilleros.get(contadorCasilleros)
												.setText(String.valueOf(tableroNuevo.posicionesEnTablero(i, j)));
										contadorCasilleros++;
									}
								}
							}
						} else {
							evt.setKeyCode(0);
						}
					}
				} else if (juegoIniciado == true && evt.getKeyCode() == KeyEvent.VK_DOWN) {

					if (tableroNuevo.juegoTermiado() == true) {
						mensajeJuegoTerminado.setVisible(true);
					} else {
						if (tableroNuevo.existenSumasVerticales() == true) {
							tableroNuevo.sumarHaciaAbajo();
							tableroNuevo.agregarAleatorio();

							int contadorCasilleros = 0;

							for (int i = 0; i < tableroNuevo.getTamanio(); i++) {

								for (int j = 0; j < tableroNuevo.posicionEnTablero(0); j++) {

									if (tableroNuevo.posicionesEnTablero(i, j) == null) {
										casilleros.get(contadorCasilleros).setText("");
										contadorCasilleros++;
									} else {
										casilleros.get(contadorCasilleros)
												.setText(String.valueOf(tableroNuevo.posicionesEnTablero(i, j)));
										contadorCasilleros++;
									}
								}
							}
						} else {
							evt.setKeyCode(0);

						}
					}
				} else if (juegoIniciado == true && evt.getKeyCode() == KeyEvent.VK_RIGHT) {

					if (tableroNuevo.juegoTermiado() == true) {
						mensajeJuegoTerminado.setVisible(true);
					} else {
						if (tableroNuevo.existenSumasHorizontales() == true) {
							tableroNuevo.sumarHaciaLaDerecha();
							tableroNuevo.agregarAleatorio();

							int contadorCasilleros = 0;

							for (int i = 0; i < tableroNuevo.getTamanio(); i++) {

								for (int j = 0; j < tableroNuevo.posicionEnTablero(0); j++) {

									if (tableroNuevo.posicionesEnTablero(i, j) == null) {
										casilleros.get(contadorCasilleros).setText("");
										contadorCasilleros++;
									} else {
										casilleros.get(contadorCasilleros)
												.setText(String.valueOf(tableroNuevo.posicionesEnTablero(i, j)));
										contadorCasilleros++;
									}
								}
							}
						} else {
							evt.setKeyCode(0);
						}
					}

				} else if (juegoIniciado == true && evt.getKeyCode() == KeyEvent.VK_LEFT) {

					if (tableroNuevo.juegoTermiado() == true) {
						mensajeJuegoTerminado.setVisible(true);
					} else {
						if (tableroNuevo.existenSumasHorizontales() == true) {
							tableroNuevo.sumarHaciaLaIzquierda();
							tableroNuevo.agregarAleatorio();

							int contadorCasilleros = 0;

							for (int i = 0; i < tableroNuevo.getTamanio(); i++) {

								for (int j = 0; j < tableroNuevo.posicionEnTablero(0); j++) {

									if (tableroNuevo.posicionesEnTablero(i, j) == null) {
										casilleros.get(contadorCasilleros).setText("");
										contadorCasilleros++;
									} else {
										casilleros.get(contadorCasilleros)
												.setText(String.valueOf(tableroNuevo.posicionesEnTablero(i, j)));
										contadorCasilleros++;
									}
								}
							}
						} else {
							evt.setKeyCode(0);
						}
					}
				}
			}
		});

		// Label que contiene mensaje de juego terminado
		mensajeJuegoTerminado = new JLabel("Perdiste!");
		mensajeJuegoTerminado.setForeground(SystemColor.windowBorder);
		mensajeJuegoTerminado.setHorizontalAlignment(SwingConstants.CENTER);
		mensajeJuegoTerminado.setFont(new Font("Arial", Font.BOLD, 45));
		mensajeJuegoTerminado.setBounds(135, 340, 245, 58);
		juego.getContentPane().add(mensajeJuegoTerminado);
		mensajeJuegoTerminado.setVisible(false);

		// Label que contiene el numero de un casillero
		casilleroUno = new JLabel("");
		casilleroUno.setHorizontalAlignment(SwingConstants.CENTER);
		casilleroUno.setForeground(new Color(255, 99, 71));
		casilleroUno.setFont(new Font("Arial", Font.BOLD, 45));
		casilleroUno.setBounds(10, 120, 120, 120);
		casilleroUno.setOpaque(true);
		juego.getContentPane().add(casilleroUno);

		// Label que contiene el numero de un casillero
		casilleroDos = new JLabel("");
		casilleroDos.setHorizontalAlignment(SwingConstants.CENTER);
		casilleroDos.setForeground(new Color(255, 99, 71));
		casilleroDos.setFont(new Font("Arial", Font.BOLD, 45));
		casilleroDos.setBounds(135, 120, 120, 120);
		casilleroDos.setOpaque(true);
		juego.getContentPane().add(casilleroDos);

		// Label que contiene el numero de un casillero
		casilleroTres = new JLabel("");
		casilleroTres.setHorizontalAlignment(SwingConstants.CENTER);
		casilleroTres.setForeground(new Color(255, 99, 71));
		casilleroTres.setFont(new Font("Arial", Font.BOLD, 45));
		casilleroTres.setBounds(260, 120, 120, 120);
		casilleroTres.setOpaque(true);
		juego.getContentPane().add(casilleroTres);

		// Label que contiene el numero de un casillero
		casilleroCuatro = new JLabel("");
		casilleroCuatro.setHorizontalAlignment(SwingConstants.CENTER);
		casilleroCuatro.setForeground(new Color(255, 99, 71));
		casilleroCuatro.setFont(new Font("Arial", Font.BOLD, 45));
		casilleroCuatro.setBounds(385, 120, 120, 120);
		casilleroCuatro.setOpaque(true);
		juego.getContentPane().add(casilleroCuatro);

		// Label que contiene el numero de un casillero
		casilleroCinco = new JLabel("");
		casilleroCinco.setHorizontalAlignment(SwingConstants.CENTER);
		casilleroCinco.setForeground(new Color(255, 99, 71));
		casilleroCinco.setFont(new Font("Arial", Font.BOLD, 45));
		casilleroCinco.setBounds(10, 245, 120, 120);
		casilleroCinco.setOpaque(true);
		juego.getContentPane().add(casilleroCinco);

		// Label que contiene el numero de un casillero
		casilleroSeis = new JLabel("");
		casilleroSeis.setHorizontalAlignment(SwingConstants.CENTER);
		casilleroSeis.setForeground(new Color(255, 99, 71));
		casilleroSeis.setFont(new Font("Arial", Font.BOLD, 45));
		casilleroSeis.setBounds(135, 245, 120, 120);
		casilleroSeis.setOpaque(true);
		juego.getContentPane().add(casilleroSeis);

		// Label que contiene el numero de un casillero
		casilleroSiete = new JLabel("");
		casilleroSiete.setHorizontalAlignment(SwingConstants.CENTER);
		casilleroSiete.setForeground(new Color(255, 99, 71));
		casilleroSiete.setFont(new Font("Arial", Font.BOLD, 45));
		casilleroSiete.setBounds(260, 245, 120, 120);
		casilleroSiete.setOpaque(true);
		juego.getContentPane().add(casilleroSiete);

		// Label que contiene el numero de un casillero
		casilleroOcho = new JLabel("");
		casilleroOcho.setHorizontalAlignment(SwingConstants.CENTER);
		casilleroOcho.setForeground(new Color(255, 99, 71));
		casilleroOcho.setFont(new Font("Arial", Font.BOLD, 45));
		casilleroOcho.setBounds(385, 245, 120, 120);
		casilleroOcho.setOpaque(true);
		juego.getContentPane().add(casilleroOcho);

		// Label que contiene el numero de un casillero
		casilleroNueve = new JLabel("");
		casilleroNueve.setHorizontalAlignment(SwingConstants.CENTER);
		casilleroNueve.setForeground(new Color(255, 99, 71));
		casilleroNueve.setFont(new Font("Arial", Font.BOLD, 45));
		casilleroNueve.setBounds(10, 370, 120, 120);
		casilleroNueve.setOpaque(true);
		juego.getContentPane().add(casilleroNueve);

		// Label que contiene el numero de un casillero
		casilleroDiez = new JLabel("");
		casilleroDiez.setHorizontalAlignment(SwingConstants.CENTER);
		casilleroDiez.setForeground(new Color(255, 99, 71));
		casilleroDiez.setFont(new Font("Arial", Font.BOLD, 45));
		casilleroDiez.setBounds(135, 370, 120, 120);
		casilleroDiez.setOpaque(true);
		juego.getContentPane().add(casilleroDiez);

		// Label que contiene el numero de un casillero
		casilleroOnce = new JLabel("");
		casilleroOnce.setHorizontalAlignment(SwingConstants.CENTER);
		casilleroOnce.setForeground(new Color(255, 99, 71));
		casilleroOnce.setFont(new Font("Arial", Font.BOLD, 45));
		casilleroOnce.setBounds(260, 370, 120, 120);
		casilleroOnce.setOpaque(true);
		juego.getContentPane().add(casilleroOnce);

		// Label que contiene el numero de un casillero
		casilleroDoce = new JLabel("");
		casilleroDoce.setHorizontalAlignment(SwingConstants.CENTER);
		casilleroDoce.setForeground(new Color(255, 99, 71));
		casilleroDoce.setFont(new Font("Arial", Font.BOLD, 45));
		casilleroDoce.setBounds(385, 370, 120, 120);
		casilleroDoce.setOpaque(true);
		juego.getContentPane().add(casilleroDoce);

		// Label que contiene el numero de un casillero
		casilleroTrece = new JLabel("");
		casilleroTrece.setHorizontalAlignment(SwingConstants.CENTER);
		casilleroTrece.setForeground(new Color(255, 99, 71));
		casilleroTrece.setFont(new Font("Arial", Font.BOLD, 45));
		casilleroTrece.setBounds(10, 495, 120, 120);
		casilleroTrece.setOpaque(true);
		juego.getContentPane().add(casilleroTrece);

		// Label que contiene el numero de un casillero
		casilleroCatorce = new JLabel("");
		casilleroCatorce.setHorizontalAlignment(SwingConstants.CENTER);
		casilleroCatorce.setForeground(new Color(255, 99, 71));
		casilleroCatorce.setFont(new Font("Arial", Font.BOLD, 45));
		casilleroCatorce.setBounds(135, 495, 120, 120);
		casilleroCatorce.setOpaque(true);
		juego.getContentPane().add(casilleroCatorce);

		// Label que contiene el numero de un casillero
		casilleroQuince = new JLabel("");
		casilleroQuince.setHorizontalAlignment(SwingConstants.CENTER);
		casilleroQuince.setForeground(new Color(255, 99, 71));
		casilleroQuince.setFont(new Font("Arial", Font.BOLD, 45));
		casilleroQuince.setBounds(260, 495, 120, 120);
		casilleroQuince.setOpaque(true);
		juego.getContentPane().add(casilleroQuince);

		// Label que contiene el numero de un casillero
		casilleroDieciseis = new JLabel("");
		casilleroDieciseis.setHorizontalAlignment(SwingConstants.CENTER);
		casilleroDieciseis.setForeground(new Color(255, 99, 71));
		casilleroDieciseis.setFont(new Font("Arial", Font.BOLD, 45));
		casilleroDieciseis.setBounds(385, 495, 120, 120);
		casilleroDieciseis.setOpaque(true);
		juego.getContentPane().add(casilleroDieciseis);
	}
}